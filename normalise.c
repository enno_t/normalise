// SPDX-FileCopyrightText: 2022 Enno Tensing <enno@tensing.me>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
static const char *UTF8[] = { "Ä", "ä", "Ö", "ö", "Ü", "ü", "ß" };

void *_realloc(void *buf, int nmeb, int size);
char *normalise(char *str, char *new);
size_t cnt_size(char *str);
size_t _strlen(const char *);

#define strlen(x) _strlen(x)

size_t _strlen(const char *s)
{
	size_t i;

	for (i = 0; s[i] != '\0'; ++i)
		;

	return i;
}


void *_realloc(void *buf, int nmeb, int size)
{
	free(buf);

	void *new = calloc(nmeb, size);

	return new;
}

char *normalise(char *str, char *new)
{
	int special = 0;
	char special_start = UTF8[0][0]; // The same for all of them
	size_t len = strlen(str);
	for (size_t i = 0; i < len; ++i) {
		if (special) {
			switch (str[i]) {
			case  0xFFFFFF84: // Ä
				new[i - 1] = 'A';
				new[i] = 'E';
				break;
			case  0xFFFFFFA4: // Ä
				new[i - 1] = 'a';
				new[i] = 'e';
				break;
			case  0xFFFFFF96: // Ö
				new[i - 1] = 'O';
				new[i] = 'E';
				break;
			case  0xFFFFFFB6: // ö
				new[i - 1] = 'o';
				new[i] = 'e';
				break;
			case  0xFFFFFF9C: // Ü
				new[i - 1] = 'U';
				new[i] = 'E';
				break;
			case  0xFFFFFFBC: // ü
				new[i - 1] = 'u';
				new[i] = 'e';
				break;
			case  0xFFFFFF9F: // ß
				new[i - 1] = 's';
				new[i] = 's';
				break;
			default:
				break;
			}
			special = 0;
		} else if (special_start == str[i]) {
			special = 1;
		} else if (str[i] == ' ' || str[i] == '\t' || str[i] == '\'' || str[i] == '\"') {
			new[i] = '_';
		} else {
			new[i] = str[i];
		}
	}

	return new;
}

size_t cnt_size(char *str)
{
	size_t len = strlen(str);
	size_t cnt = 0;

	int special = 0;

	for (size_t i = 0; i < len; ++i) {
		if (str[i] == ' ' || str[i] == '\t' || str[i] == '\'' || str[i] == '\"') {
			++cnt;
		}

		if (special && str[i] == UTF8[0][1]) {
			++cnt;
			special = 0;
		} else if (special && str[i] == UTF8[1][1]) {
			++cnt;
			special = 0;
		} else if (special && str[i] == UTF8[2][1]) {
			++cnt;
			special = 0;
		} else if (special && str[i] == UTF8[3][1]) {
			++cnt;
			special = 0;
		} else if (special && str[i] == UTF8[4][1]) {
			++cnt;
			special = 0;
		} else if (special && str[i] == UTF8[5][1]) {
			++cnt;
			special = 0;
		} else if (special && str[i] == UTF8[6][1]) {
			++cnt;
			special = 0;
		}

		if (UTF8[0][0] == str[i]) {
			special = 1;
		}
	}

	return cnt;
}

int main(int argc, char **argv)
{
	if (argc > 1) {
		char *str = calloc(2, sizeof(char));
		size_t size = 0;
		for (int i = 1; i < argc; ++i) {
			size = cnt_size(argv[i]);
			str = _realloc(str, size + 1, sizeof(char));

			if (!str) {
				perror("normalise");
				free(str);
				return 1;
			}

			str = normalise(argv[i], str);
			printf("%s\n", str);
		}
		free(str);
	} else {
		char *in = calloc((1024 * 4) + 1, sizeof(char));
		size_t size = 0;

		if (!in) {
			perror("normalise");
			free(in);
			return 1;
		}

		char *str = calloc(2, sizeof(char));
		while (read(STDIN_FILENO, in, sizeof(in)) > 0) {
			size = cnt_size(in);
			str = _realloc(str, size + 1, sizeof(char));

			if (!str) {
				perror("normalise");
				free(str);
				free(in);
				return 1;
			}

			str = normalise(in, str);
			printf("%s", str);
		}
		printf("\n");
		free(in);
		free(str);
	}
	return 0;
}
