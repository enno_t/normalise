// SPDX-FileCopyrightText: 2022 Enno Tensing <enno@tensing.me>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

void *_realloc(void *buf, int nmeb, int size);
char *escape(char *str, char *new);
int count_replacements(char *);

static char LESS_THAN = '<';
static char GREATER_THAN = '>';
static char QUOTATION_MARK = '\"';
static char APOSTROPHE = '\'';
static char BACKSLASH = '\\';

void *_realloc(void *buf, int nmeb, int size)
{
	free(buf);

	void *new = calloc(nmeb, size);

	return new;
}

char *escape(char *str, char *new)
{
	size_t j = 0;
	size_t len = strlen(str);
	for (size_t i = 0; i < len; ++i) {
		if (str[i] == LESS_THAN) {
			new[i + j] = BACKSLASH;
			new[i + j + 1] = LESS_THAN;
			++j;
		} else if (str[i] == GREATER_THAN) {
			new[i + j] = BACKSLASH;
			new[i + j + 1] = GREATER_THAN;
			++j;
		} else if (str[i] == QUOTATION_MARK) {
			new[i + j] = BACKSLASH;
			new[i + j + 1] = QUOTATION_MARK;
			++j;
		} else if (str[i] == APOSTROPHE) {
			new[i + j] = BACKSLASH;
			new[i + j + 1] = APOSTROPHE;
			++j;
		} else {
			new[i + j] = str[i];
		}
	}

	return new;
}

int count_replacements(char *str)
{
	size_t len = strlen(str);

	int cnt = 0;
	for (size_t i = 0; i < len; ++i) {
		if (str[i] == '<' || str[i] == '>' || str[i] == '\"' || str[i] == '\'')
			++cnt;
	}

	return cnt;
}

int main(int argc, char **argv)
{
	if (argc > 1) {
		size_t length;
		char *str = calloc(2, sizeof(char));
		for (int i = 1; i < argc; ++i) {
			length = strlen(argv[i]);
			str = _realloc(str, (length + count_replacements(argv[i])) + 1, sizeof(char));
			
			if (errno == ENOMEM) {
				perror("escape - argv");
				free(str);
				return 1;
			}
		
			str = escape(argv[i], str);
			printf("%s\n", str);
		}

		free(str);
	} else {
		char *in = calloc((1024 * 4) + 1, sizeof(char));
		if (errno == ENOMEM) {

			perror("escape - stdin");

			free(in);
			return 1;
		}

		char *str = calloc(2, sizeof(char));
		size_t length;
		while (read(STDIN_FILENO, in, sizeof(in)) > 0) {
			length = strlen(in);
			str = _realloc(str, (length + count_replacements(in)) + 1, sizeof(char));
			if (errno == ENOMEM) {

				perror("escape - stdin");

				free(str);
				free(in);
				return 1;
			}

			str = escape(in, str);
			printf("%s", str);
		}
		printf("\n");
		free(in);
		free(str);
	}
	return 0;
}
