CC=gcc
CFLAGS=-Wall -Wextra -std=c99 \
       -pipe -march=native -O2 \
       -D_FORTIFY_SOURCE=2 \
       -fstack-protector-strong \
       -fcf-protection

all: normalise escape

normalise: normalise.c
	@echo CC $^
	@$(CC) -o $@ $^ $(CLFAGS)

escape: escape.c
	@echo CC $^
	@$(CC) -o $@ $^ $(CFLAGS)

clean:
	@echo Cleaning dir...
	@rm -f normalise escape
